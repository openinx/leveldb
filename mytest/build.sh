#!/bin/bash

BASE_PATH=`readlink -f ..`
cd $BASE_PATH;
make
cd $BASE_PATH/mytest
export LD_LIBRARY_PATH=$BASE_PATH:$LD_LIBRARY_PATH
g++ -Wall -ggdb -O0 -I$BASE_PATH/include -L$BASE_PATH demo.cpp \
    -lleveldb -lrt -o demo

g++ -Wall -ggdb -O0 -I$BASE_PATH/include -I$BASE_PATH -L$BASE_PATH histogramTest.cpp \
    -lleveldb -lrt -o histogram 

g++ -Wall -ggdb -O0 -I$BASE_PATH/include -I$BASE_PATH -L$BASE_PATH blockTest.cpp \
    -lleveldb -lrt -o block
