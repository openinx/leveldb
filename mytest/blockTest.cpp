#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "leveldb/db.h"
#include "table/block_builder.h"
#include "table/block.h"
#include "leveldb/slice.h"

using namespace std;

string getRandomStr(int len){
	static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    static const int alphSize = sizeof(alphanum) / sizeof(char);
    char *s = new char[len+1];
    for(int i = 0 ; i < len ; ++ i)
    	s[i]= alphanum[rand() % alphSize];
    s[len] = 0;
    return string(s);
}

int main(){
	map<string, string>  m; 
	static const int max_size = 100000;
	static const int max_len = 10;
	for(int i = 0 ; i < max_size; ++ i){
		string key = getRandomStr(max_len);
		string val = getRandomStr(max_len);
		m[key] = val;
	}
	leveldb::Options options;
	leveldb::BlockBuilder *bb = new leveldb::BlockBuilder(&options);
	for(map<string,string>::iterator it = m.begin(); it != m.end(); it ++ ){
		bb->Add(leveldb::Slice(it->first), leveldb::Slice(it->second));
	}
	bb->Finish();
	cout << bb->CurrentSizeEstimate()  << endl;
	return 0 ;
}
