#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "leveldb/db.h"
#include "leveldb/comparator.h"
#include "leveldb/iterator.h"

using namespace std;

leveldb::DB *ldb;
std::string data_dir = "./data";

int init() {
    leveldb::Options init_options;
    init_options.create_if_missing = true;
    leveldb::Status s = leveldb::DB::Open(init_options, data_dir.c_str(), &ldb);
    if (!s.ok()) {
        printf("db open failed:%s\n", s.ToString().c_str());
        return -1;
    }
    return 0;
}

void clear(){
	leveldb::Options options;
	options.create_if_missing = true;
	leveldb::Status s = leveldb::DestroyDB(data_dir, options);
	if(!s.ok()){
		printf("db destroy failed: %s\n", data_dir.c_str());
	}
	delete ldb;
}

string getRandomStr(int len){
	static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    static const int alphSize = sizeof(alphanum) / sizeof(char);
    char *s = new char[len+1];
    for(int i = 0 ; i < len ; ++ i)
    	s[i]= alphanum[rand() % alphSize];
    s[len] = 0;
    return string(s);
}

bool write(std::string key) {
    leveldb::Slice value = leveldb::Slice(key.data(), key.size());
    leveldb::Status s = ldb->Put(leveldb::WriteOptions(),
        leveldb::Slice(key.data(), key.size()), value);
    return s.ok();
}

bool read(std::string key) {
    std::string value;
    leveldb::Status s = ldb->Get(leveldb::ReadOptions(),
        leveldb::Slice(key.data(), key.size()), &value);
    return s.ok();
}


#define MAX_WRITE_COUNT (100000000)
void testWriteSpeed(){
	// set rand seed.
	int start, take ;
	cout << "Test Write Speed ... " << endl;
	srand(time(NULL));
	start = clock();
	for(int i = 0 ; i < MAX_WRITE_COUNT; ++ i){
		string key = getRandomStr(10);
		write(key);
	}
	take = clock() - start;
	printf("WriteSpeed: %f ms per write.\n",
		      (float)take*1000/CLOCKS_PER_SEC/MAX_WRITE_COUNT);
}


#define MAX_READ_COUNT (MAX_WRITE_COUNT)
void testReadSpeed(){
	int start, take;
	cout << "Test Read Speed ... " << endl;
	srand(time(NULL));
	start = clock();
	for(int i = 0 ; i < MAX_READ_COUNT; ++ i){
		string key = getRandomStr(10);
		read(key);
	}
	take = clock() - start;
	printf("ReadSpeed: %f ms per read.\n", 
		      (float)take*1000/CLOCKS_PER_SEC/MAX_READ_COUNT);
}


void testIterator(){
	int cnt = 0 ;
	leveldb::Iterator* it = ldb->NewIterator(leveldb::ReadOptions());
	for(it->SeekToFirst(); it->Valid(); it->Next()){
		printf("key: %s,  value: %s\n",
			       it->key().ToString().c_str(),
			       it->value().ToString().c_str());
		++ cnt;
	}
	cout << "Total: " << cnt << endl;
	delete it;
}


void testRange(){
	leveldb::Slice start = "a";
	leveldb::Slice limit = "zzzzzzzzzzzzzzzzzzz";
	leveldb::Range range(start, limit);
	leveldb::Range *ranges;
	uint64_t size[2];
	ranges = &range ;
	ldb->GetApproximateSizes(ranges, 1 , size);
	printf("size: %lu\n", size[0]);
}


void testCompaction(){
	int start = clock();
	printf("Compact all database ...\n");
	ldb->CompactRange(NULL, NULL);
	printf("Compact finished. take %f secs\n",
				(float)(clock()-start)/CLOCKS_PER_SEC);
}

int main(){
	srand(time(NULL));
	if(init() < 0){
		perror("init failed.");
		exit(EXIT_FAILURE);
	}
	testWriteSpeed();
	testReadSpeed();
	testIterator();
	testRange();
	testCompaction();
	clear();
	return 0;
}
