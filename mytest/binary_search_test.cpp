#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;

// find smallest index i such that val <= a[i]
int binary_search(int a[], int n , int val){
	int left = 0 , right = n - 1 , mid; 
	if(n > 0 && val > a[n-1]) return n ;
	while( left < right ){
		mid = ( left + right) >> 1 ;
		if(val > a[mid]){
			left = mid + 1 ;
		}else{
			right = mid ;
		}
	}
	return right;
}

int main(){
	int a[] = { 1 , 5 , 8 , 19 , 34, 35 , 41, 41, 45, 568};
	assert(binary_search(a, sizeof(a)/sizeof(int), 0) == 0);
	assert(binary_search(a, sizeof(a)/sizeof(int), 1) == 0);
	assert(binary_search(a, sizeof(a)/sizeof(int), 2) == 1);
	assert(binary_search(a, sizeof(a)/sizeof(int), 4) == 1);
	assert(binary_search(a, sizeof(a)/sizeof(int), 5) == 1);
	assert(binary_search(a, sizeof(a)/sizeof(int), 7) == 2);
	assert(binary_search(a, sizeof(a)/sizeof(int), 42) == 8);
	assert(binary_search(a, sizeof(a)/sizeof(int), 567) == 9);
	assert(binary_search(a, sizeof(a)/sizeof(int), 569) == 10);
	return 0;
}