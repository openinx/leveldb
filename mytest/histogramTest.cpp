#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "util/histogram.h"
#include "util/random.h"

using namespace std;

#define MAX_SZ  1000000

int main(int argc, char **argv){
	leveldb::Histogram h;
	srand(time(NULL));
	h.Clear();
	for(int i = 0 ; i < MAX_SZ; ++ i){
		h.Add((double)(rand() % 5));
	}
	cout << h.ToString() << endl;

	leveldb::Random random(time(NULL));
	for(int i = 0 ;i < 10 ; ++ i)
		cout << random.Next() << endl;
	return 0 ;
}
